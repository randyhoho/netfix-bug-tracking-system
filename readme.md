# netfix-bug-tracking-system

## Name
Netfix

## Description
netfix is a bug tracking web application which monitors detected bugs. It also is a reporting tool, so that it acts as a middleman between programmer and users.

## Main features:
1. Bug tracking recording
2. Exclusive login content

## Installation
`git clone git@gitlab.com:randyhoho/netfix-bug-tracking-system.git`

## Authors and acknowledgment
Randy Ho, Angela Wong, Enson Yeung
