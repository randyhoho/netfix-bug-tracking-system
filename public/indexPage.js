const welcome = document.querySelector('#welcome')
const generalUser = document.querySelector('#generalUser')
const user = document.querySelector('.user')

document.body.addEventListener('mousemove', function(event){
    welcome.style.top = event.pageY + 'px';
    welcome.style.left = event.pageX + 'px';

    generalUser.style.top = (event.pageY - user.offsetTop) + 'px';
    generalUser.style.left = event.pageX + 'px';
});

// item js

const nameNetfix = document.querySelector('.nameNetfix')
const login = document.querySelector('.loginLogin')
const register = document.querySelector('.registerRegister')
const about = document.querySelector('.aboutUs')
const phone = document.querySelector('.phone')
const address = document.querySelector('.address')
const backArea = document.querySelector('.backArea')

const one = document.querySelector('.one')
const two = document.querySelector('.two')
const three = document.querySelector('.three')
const four = document.querySelector('.four')
const five = document.querySelector('.five')
const six = document.querySelector('.six')

backArea.addEventListener('mousemove', function(event){
    one.classList.remove('active');
    two.classList.remove('active');
    three.classList.remove('active');
    four.classList.remove('active');
    five.classList.remove('active');
    six.classList.remove('active');
});

nameNetfix.addEventListener('mousemove', function(event){
    one.classList.add('active');
    two.classList.remove('active');
    three.classList.remove('active');
    four.classList.remove('active');
    five.classList.remove('active');
    six.classList.remove('active');
});

login.addEventListener('mousemove', function(event){
    one.classList.remove('active');
    two.classList.add('active');
    three.classList.remove('active');
    four.classList.remove('active');
    five.classList.remove('active');
    six.classList.remove('active');
});

register.addEventListener('mousemove', function(event){
    one.classList.remove('active');
    two.classList.remove('active');
    three.classList.add('active');
    four.classList.remove('active');
    five.classList.remove('active');
    six.classList.remove('active');
});

about.addEventListener('mousemove', function(event){
    one.classList.remove('active');
    two.classList.remove('active');
    three.classList.remove('active');
    four.classList.add('active');
    five.classList.remove('active');
    six.classList.remove('active');
});

phone.addEventListener('mousemove', function(event){
    one.classList.remove('active');
    two.classList.remove('active');
    three.classList.remove('active');
    four.classList.remove('active');
    five.classList.add('active');
    six.classList.remove('active');
});

address.addEventListener('mousemove', function(event){
    one.classList.remove('active');
    two.classList.remove('active');
    three.classList.remove('active');
    four.classList.remove('active');
    five.classList.remove('active');
    six.classList.add('active');
});




// backend js


let allForm = document.querySelectorAll(".loginFormAll");


for (let obj of allForm){

    obj.addEventListener("submit", async function (event) {
        event.preventDefault();
    
        const form = event.target;
        const forObj = {
            username: form.username.value,
            password: form.password.value
        }
        console.log(forObj);
    
        const res = await fetch('/home',{
            method:"POST",
            headers: {
                "Content-Type": "application/json"
            }, 
            body: JSON.stringify(forObj)
        })
    
    
        const result = await res.json();
        console.log(result);
    
        if (res.status === 200){
            console.log("success")
            window.location = '/businessCard.html'
        }
        else{
            document.querySelector('.alert-container')
                .innerHTML = `<div class="alert alert-primary" role="alert">
                Login Failed ${result.msg}
            </div>`;

            document.querySelector('.alert-container2')
            .innerHTML = `<div class="alert alert-primary" role="alert">
            Login Failed ${result.msg}
        </div>`;
        }
    });
}

// socket.io
const socket = io.connect();
socket.on('new-user',(data)=>{
    console.log(data);
document.getElementById("socketBox").innerHTML += `<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-lightbulb-fill" viewBox="0 0 16 16">
<path d="M2 6a6 6 0 1 1 10.174 4.31c-.203.196-.359.4-.453.619l-.762 1.769A.5.5 0 0 1 10.5 13h-5a.5.5 0 0 1-.46-.302l-.761-1.77a1.964 1.964 0 0 0-.453-.618A5.984 5.984 0 0 1 2 6zm3 8.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1l-.224.447a1 1 0 0 1-.894.553H6.618a1 1 0 0 1-.894-.553L5.5 15a.5.5 0 0 1-.5-.5z"/>
</svg>` + data;
window.setTimeout("closeHelpDiv();", 4000);
})


function closeHelpDiv(){
document.getElementById("socketBox").style.display=" none";
}



