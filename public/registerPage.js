const signinBtn = document.querySelector('.signinBtn');
const signupBtn = document.querySelector('.signupBtn');
const formBx = document.querySelector('.formBx');
const loginBox = document.querySelector('.loginBox');
const one = document.querySelector('.one')
const two = document.querySelector('.two')

signupBtn.onclick = function(){
    formBx.classList.add('active');
    loginBox.classList.add('active');
    two.classList.remove('active');
}

signinBtn.onclick = function(){
    formBx.classList.remove('active');
    loginBox.classList.remove('active');
    two.classList.add('active');
}


let role = document.getElementById("roleChosen");

role.addEventListener("change", () =>{
    console.log(role.value)
})



// JS ajax function

let allForm = document.querySelectorAll(".loginFormAll");


for (let obj of allForm){

    obj.addEventListener("submit", async function (event) {
        event.preventDefault();

        const form = event.target;
        const forObj = {
            first_name: form.first_name.value,
            last_name: form.last_name.value,
            user_name: form.user_name.value,
            password: form.password.value,
            role: form.role.value,
            email_address: form.email_address.value
        }
        console.log(forObj);
    
        const res = await fetch('/register',{
            method:"POST",
            headers: {
                "Content-Type": "application/json"
            }, 
            body: JSON.stringify(forObj)
        })
    
    
        const result = await res.json();
        console.log(result);
    
        if (res.status === 200){
            console.log("success")
            window.location = '/home.html'
        }
        else{
            document.querySelector('.alert-container')
                .innerHTML = `<div class="alert alert-warning" role="alert">
                Register failed. ${result.msg}
            </div>`;
        }
    });
}
