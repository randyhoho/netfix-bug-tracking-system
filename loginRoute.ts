import express, { NextFunction } from 'express';
import { client } from './app';
import { checkPassword } from './hash';
import { Request, Response } from 'express';
import path from 'path';


export const loginRoute = express.Router();

loginRoute.post('/home', async (req, res) => {
    const { username, password } = req.body;
    let users: User[] = (await client.query('SELECT * from users where user_name = $1',
        [username])).rows;
    const user = users[0];
    if (user && await checkPassword(password, user.password)) {
        req.session['user'] = user;
        console.log(req.session['user'])
        res.status(200).json({ success: true })
    } else {
        res.status(401).json({ success: false, msg: "Username/Password is incorrect!" })
    }
});

export interface User {
    username: string;
    password: string
}

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.session['user']) {
        next();
    } else {
        res.sendFile(path.resolve('./public/404.html'))
    }
}

export function isLoggedInAPI(req: Request, res: Response, next: NextFunction) {
    if (req.session['user']) {
        next();
    } else {
        res.status(401).json({ msg: "UnAuthorized" });
    }
}

loginRoute.post('/businessCardLogin', async function (req, res) {
    const userData = req.session['user'];
    console.log(userData)
    res.json({userData})
})
