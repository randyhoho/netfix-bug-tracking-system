import express, { Request, Response } from 'express';
import { client } from './app';

export const bugTable = express.Router();
bugTable.get('/g', getBugTable);
bugTable.get('/g/:id', getBugTableById); //not yet call but tested.
bugTable.post('/p', postBugTable);
bugTable.put('/put/:id', putBugTable);
bugTable.delete('/d/:id', deleteBugTable);

async function getBugTable(req: Request, res: Response) {
    try {
        res.json((await client.query(
            `SELECT * FROM issue_reports`)).rows);
    } catch (e) {
        console.error(e)
        res.status(500).json({ message: "Internal Server Error" })
    }
}

async function getBugTableById(req: Request, res: Response) {
    try {
        const id = parseInt(req.params.id);
        res.json(
            (await client.query(`SELECT * FROM issue_reports WHERE id = $1`, [id]))
                .rows,
        )
    } catch (e) {
        console.error(e)
        res.status(500).json({message: "Internal Server Error"})
    }
}

async function postBugTable(req: Request, res: Response) {
    try {
        const { creator_id, title, description, request_type, priority, browser, status, assignee_id, project_id } = req.body;
        res.json(
            (
                await client.query(
                    ` INSERT INTO issue_reports (creator_id,title,description,request_type,priority,browser,status,assignee_id,project_id) values (
                    $1,$2,$3,$4,$5,$6,$7,$8,$9) `,
                    [creator_id, title, description, request_type, priority, browser, status, assignee_id, project_id],
                )
            ).rowCount)
    } catch (e) {
        console.error(e)
        res.status(500).json({ message: "Internal Server Error" })
    }
}

async function putBugTable(req: Request, res: Response) {
    try {
        const id = parseInt(req.params.id);
        console.log(`updating id ${id}`) //may display in nodeJs
        console.log(req.body) //may display in nodeJs
        const { creator_id, title, description, request_type, priority, browser, status, assignee_id, project_id } = req.body;
        res.json(
            (
                await client.query(
                    `UPDATE issue_reports set 
                creator_id = $1, title = $2, description = $3, 
                request_type = $4, priority = $5, browser = $6, 
                status = $7, assignee_id = $8, project_id = $9 
                where id = $10`,
                    [creator_id, title, description, request_type, priority, browser, status, assignee_id, project_id, id],
                )
            ).rowCount,
        );
    } catch (e) {
        console.error(e)
        res.status(500).json({ message: "Internal Server Error" })
    }

}

async function deleteBugTable(req: Request, res: Response) {
    try {
        const id = parseInt(req.params.id);
        // console.log(`deleting ${id}`) //may display 
        res.json(
            (await client.query(
                `DELETE FROM  issue_reports  where id = $1`, [id]))
                .rowCount,
        );
    } catch (e) {
        console.error(e)
        res.status(500).json({ message: "Internal Server Error" })
    }
}