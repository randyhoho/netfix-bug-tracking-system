import express from 'express';
import { client, io } from './app';
import { hashPassword } from './hash';
import { User } from './loginRoute';


export const googleLoginRoute = express.Router();

googleLoginRoute.route('/googleLogin')
    .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
        res.sendFile(`${__dirname}/public/googleLogin.html`);
    })

googleLoginRoute.post('/googleLogin', async function (req, res) {
    const { user_name, role} = req.body;

    const first_name = req.query.family_name;
    const last_name = req.query.given_name;
    const email_address = req.query.email;
    // req.body.push(req.query.email)
    console.log(req.body, first_name,last_name,email_address)
    const password = req.body.password;
    const afterHashPassword = await hashPassword(password)
    const result = await client.query('SELECT user_name from users WHERE user_name = $1 or email_address = $2',[user_name, email_address]) 
    if ( !user_name || !role || !password) {
        res.status(409).json({ success: false, msg: "You should enter all info." })
        return;
    }
    if (result.rows.length >= 1 ) {
        res.status(409).json({ success: false, msg: "Your username already exists" })
    } else {
        io.emit('new-user', 'One more user join Netfix.');
        let user: User[] = (await client.query('SELECT * from users where user_name = $1',
        [user_name])).rows;
        req.session['user'] = req.body;
        req.session['user'].email_address = email_address; 
                console.log(user);
                console.log(req.body);
        await client.query(`INSERT INTO users (user_name,first_name,last_name,email_address,role,password) VALUES ($1,$2,$3,$4,$5,$6)`,
            [user_name, first_name, last_name, email_address, role, afterHashPassword])
        res.status(200).json({ success: true });
        
    }
})
