import express from 'express';
import grant from 'grant';
import fetch from 'node-fetch'
import dotenv from 'dotenv';
import { Request, Response } from 'express';
import { client } from './app';

dotenv.config();

export const googleRoute = express.Router();

const grantExpress = grant.express({
    "defaults":{
        "origin": "http://localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile","email"],
        "callback": "/login/google"
    }
});

googleRoute.use(grantExpress);

googleRoute.get('/login/google',loginGoogle);

async function loginGoogle (req: Request, res: Response){
    const accessToken = req.session?.['grant'].response.access_token;

    const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
        method:"get",
        headers:{
            "Authorization":`Bearer ${accessToken}`
        }
    });
    const result = await fetchRes.json();
    console.log(result);
    const checkEmail = await client.query('SELECT user_name from users WHERE email_address = $1',[result.email]) 
    const userData = await client.query('SELECT * from users WHERE email_address = $1',[result.email]) 
    if (checkEmail.rows.length >= 1) {
        console.log(userData.rows[0])
        req.session['user'] = userData.rows[0];
        return res.redirect(`/protected/home.html`)
    } else {
        req.session['user'] = userData.rows[0];
    return res.redirect(`/googleLogin?family_name=${result.family_name}&given_name=${result.given_name}&email=${result.email}`)
    }
}

