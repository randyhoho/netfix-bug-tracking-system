import express from 'express'
import { Client } from 'pg';
import dotenv from 'dotenv';
import { bugTable } from './bugTableRoutes';
import jsonfile from 'jsonfile';


// import { NextFunction, Request, Response } from 'express';
import expressSession from 'express-session';
// import path from 'path';
import { expressSessionRoute } from './expressSessionRoute';
import { googleRoute } from './googleRoute';
import { registerPageRoute } from './registerPageRoute';
import { loginRoute, isLoggedIn, User } from './loginRoute';
import { logoutRoute } from './logoutRoute';
import { pageOf404Route } from './pageOf404Route';
import { googleLoginRoute } from './googleLoginRoute';
import {Server as SocketIO } from 'socket.io';
import http from 'http';



const PORT = 8080;
const app = express();
dotenv.config();

app.use('/', expressSessionRoute)

app.use(expressSession({ //enson's code
    secret: "Netfix",
    resave: true,
    saveUninitialized: true
}));

app.use((req, res, next) => { //enson's code
    if (!req.session['counter']) {
        req.session['counter'] = 0;
    }
    req.session['counter'] += 1;

    console.log(req.session);
    next();
});

function pad(num: number) { //enson's code
    return (num + "").padStart(2, "0");
};


app.use((req, res, next) => { //enson's code
    const now = new Date();
    const dateString = now.getFullYear() + "-" + pad(now.getMonth() + 1) + "-"
        + pad(now.getDate()) + " "
        + pad(now.getHours()) + ":"
        + pad(now.getMinutes()) + ":"
        + pad(now.getSeconds());
    console.log(`[${dateString}] Request ${req.path}`);
    next();
});

export const client = new Client({
    host: 'localhost',
    port: 5432,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
})

client.connect()
app.use(express.json());
app.use(express.static(__dirname));
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));

//*Randy
app.use('/bugTable', bugTable);
app.use('/issue1', bugTable);

//*Enson
app.use('/', expressSessionRoute)
app.use('/', googleRoute)
app.use('/', loginRoute)
app.use('/', registerPageRoute)
app.use('/', googleLoginRoute)
app.use('/', logoutRoute)
// socketioRoute.io part
const server = http.createServer(app);
export const io = new SocketIO(server);
// socketioRoute.io part



app.post('/home', async (req, res) => { //enson's code
    const users: User[] = await jsonfile.readFile('./users.json')
    const { username, password } = req.body

    let userFound: User | null = null;
    for (let user of users) {
        if (user.username === username && user.password === password) {
            userFound = user;
            break;
        }
    }
    if (userFound) {
        req.session['user'] = userFound;
        res.status(200).json({ success: true })
    } else {
        res.status(401).json({ success: false, msg: "Username/Password is incorrect!" })
    }
});


//Randy area
app.route('/issue1')
    .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
        res.sendFile(`${__dirname}/public/issue1.html`);
    })
app.route('/bugTable')
    .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
        res.sendFile(`${__dirname}/public/firstPage.html`);
    })

app.route('/issuereport')
    .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
        res.sendFile(`${__dirname}/public/issue1.html`);
    })

app.route('/edit')
    .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
        res.sendFile(`${__dirname}/public/issue_form_for_edit.html`);
    })
app.use('/issue1', bugTable);
app.route('/issue1')
    .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
        res.sendFile(`${__dirname}/public/issue1.html`);
    })

//Angela area
// import path from 'path'
// const HOME_HTML = path.resolve('./home.html')
// app.get('/home.html',(req,res)=>{
//     console.log('Home page arrived');
// });


//*Angela

app.post('/comments', isLoggedIn, async function (req, res) {
    const content = req.body;
    console.log(content);

    await client.query(`INSERT INTO comments(content, created_at) VALUES ($1, NOW())`, [content.commentContent]);

    res.json({ success: true });
});

app.get('/comments', isLoggedIn, async function (req, res) {
    const q = req.query.q;
    console.log(q);
    const result = await client.query(`SELECT * FROM comments limit 1001`);
    let comments = result.rows;
    res.json(comments);
});
app.post('/projects', isLoggedIn, async function (req, res) {
    const projectDetail = req.body;
    console.log(projectDetail);
    const projectName = projectDetail.projectName;
    const company = projectDetail.company;
    const participants = projectDetail.participants;
    const description = projectDetail.projectDescription;

    client.query(`INSERT INTO projects(name, company, participant, description, created_at) VALUES ($1, $2, $3, $4, NOW())`,
        [projectName, company, participants, description]);
});

app.delete('/comments/:id', isLoggedIn, async function (req, res) {
    const id = parseInt(req.params.id);
    console.log(id)
    if (isNaN(id)) {
        res.status(400).json({ msg: "id is not an integer" });
        return;
    }

    await client.query('DELETE from comments where id = $1', [id]);
    res.json({ success: true });
});



app.post('/projects', isLoggedIn, async function (req, res) {
    const projectDetail = req.body;
    console.log(projectDetail);
    const projectName = projectDetail.projectName;
    const company = projectDetail.company;
    const participants = projectDetail.participants;
    const description = projectDetail.projectDescription;

    client.query(`INSERT INTO projects(name, company, participant, description, created_at) VALUES ($1, $2, $3, $4, NOW())`,
        [projectName, company, participants, description]);

    res.json({ success: true });
});

app.get('/projects', isLoggedIn, async function (req, res) {
    const q = req.query.q;
    console.log(q);

    const result = await client.query(`SELECT * from projects;`);
    let projects = result.rows;
    res.json(projects);
}); 


app.delete('/projects/:id', isLoggedIn, async function (req, res) {
    const id = parseInt(req.params.id);
    console.log(id)
    if (isNaN(id)) {
        res.status(400).json({ msg: "id is not an integer" });
        return;
    }

    await client.query('DELETE from projects where id = $1', [id])

    res.json({ success: true });
});



//Angela not yet done
app.get('/project_reported/:id', isLoggedIn, async (req,res) => {
    // const PROJECT_REPORTED = path.resolve('./public/project_reported.html')
    // res.sendFile(PROJECT_REPORTED);
    const id = req.params.id;
    
    

    const project = await client.query('SELECT * from projects where id = $1', [id]);
    res.json(project.rows);

    // res.redirect('/project_reported.html');
})



//*for angela's reference:
// app.post('/comments', async function (req, res) {
//     const content = req.body;

//     client.query(`INSERT INTO comments(content, created_at) VALUES ($1, NOW())`, [content.commentContent]);



// app.post('/projects', async function (req, res) {
//     const projectDetail = req.body;
//     console.log(projectDetail);
//     const projectName = projectDetail.projectName;
//     const company = projectDetail.company;
//     const participants = projectDetail.participants;
//     const description = projectDetail.projectDescription;

//     client.query(`INSERT INTO projects(name, company, participant, description, created_at) VALUES ($1, $2, $3, $4, NOW())`,
//         [projectName, company, participants, description]);

//     res.json({ success: true });
// })




app.use(express.static("public"));
app.use(isLoggedIn, express.static("protected"));


app.use(pageOf404Route, express.static("pageOf404Route"))

///Socket io part (need before server listen, can't change)
io.on ('connection',(socket)=> {

});


//cheange app.listen to server.listen (#socket.io need to change this)
server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`)
});