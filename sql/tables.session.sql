--應該要自己proofread一次，唔知係咪啱曬

CREATE TABLE "companies" (
  "id" serial,
  "name" text,
  PRIMARY KEY ("id")
);

CREATE TABLE "Users" (
  "id" SERIAL,
  "first_name" text,
  "Last_name" text,
  "User_name" text,
  "Email_address" varchar (255),
  "Password" varchar (255),
  "Role" text,
  "dark_mode?" boolean,
  PRIMARY KEY ("id")
);

CREATE TABLE "issue_files" (
  "id" serial,
  "issue_id" integer,
  " path" varchar(255),
  "file_type"    varchar(255),
  PRIMARY KEY ("id"),
  CONSTRAINT "FK_issue_files.issue_id"
    FOREIGN KEY ("issue_id")
      REFERENCES "Issue reports"("id")
);

CREATE TABLE "Projects" (
  "id" SERIAL,
  "name" varchar (255),
  "description" text,
  "company_id" integer,
  "creator_id" integer,
  "created_at" TIMESTAMP,
  PRIMARY KEY ("id"),
  CONSTRAINT "FK_Projects.creator_id"
    FOREIGN KEY ("creator_id")
      REFERENCES "Users"("id"),
  CONSTRAINT "FK_Projects.company_id"
    FOREIGN KEY ("company_id")
      REFERENCES "companies"("id")
);

CREATE TABLE "members" (
  "id" serial,
  "project_id" integer,
  "user_id" integer,
  PRIMARY KEY ("id"),
  CONSTRAINT "FK_members.project_id"
    FOREIGN KEY ("project_id")
      REFERENCES "Projects"("id"),
  CONSTRAINT "FK_members.user_id"
    FOREIGN KEY ("user_id")
      REFERENCES "Users"("id")
);

CREATE TABLE "Issue reports" (
  "id" SERIAL,
  "creator_id" integer,
  "Date " TIMESTAMP,
  "Title" varchar (255),
  "Description" text,
  "request_type" text,
  "priority" text,
  "browser" text,
  "Status" text,
  "assignee_id" integer,
  "project_id" integer,
  PRIMARY KEY ("id"),
  CONSTRAINT "FK_Issue reports.project_id"
    FOREIGN KEY ("project_id")
      REFERENCES "Projects"("id"),
  CONSTRAINT "FK_Issue reports.assignee_id"
    FOREIGN KEY ("assignee_id")
      REFERENCES "Users"("id"),
  CONSTRAINT "FK_Issue reports.creator_id"
    FOREIGN KEY ("creator_id")
      REFERENCES "Users"("id")
);

CREATE TABLE "Comments" (
  "issue_id" integer,
  "id" SERIAL,
  "user_id" integer,
  "Content" text,
  "Created_at" TIMESTAMP,
  PRIMARY KEY ("id"),
  CONSTRAINT "FK_Comments.issue_id"
    FOREIGN KEY ("issue_id")
      REFERENCES "Issue reports"("id"),
  CONSTRAINT "FK_Comments.user_id"
    FOREIGN KEY ("user_id")
      REFERENCES "Users"("id")
);

CREATE TABLE "Notifications" (
  "id" SERIAL,
  "user_id" integer,
  "content" text,
  "Date" TIMESTAMP,
  PRIMARY KEY ("id"),
  CONSTRAINT "FK_Notifications.user_id"
    FOREIGN KEY ("user_id")
      REFERENCES "Users"("id")
);

