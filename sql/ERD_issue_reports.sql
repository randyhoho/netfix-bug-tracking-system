CREATE TABLE "issue_reports" (
  "id" SERIAL PRIMARY KEY,
  "creator_id" integer,
  "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  "title" varchar (255),
  "description" text,
  "request_type" text,
  "priority" text,
  "browser" text,
  "status" text,
  "assignee_id" integer,
  "project_id" integer
);

INSERT INTO issue_reports (creator_id,title,description,request_type,priority,browser,status,assignee_id,project_id) VALUES
    (551,'1frontend bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (552,'f2rontend bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (525,'fr3ontend bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (535,'fro4ntend bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (554,'fron5tend bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (545,'fron6tend bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (525,'front7end bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (555,'fronte8nd bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (556,'fronten9d bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (515,'frontend0 bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (559,'frontend 1bugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (055,'frontend b2ugs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666),
    (595,'frontend bu3gs','once upon a time, ......','what_s that?','low','IE','unsolved',22,666);


SELECT * FROM issue_reports;

