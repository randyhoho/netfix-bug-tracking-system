CREATE TABLE projects (
  id SERIAL primary key,
  name varchar(255) not null,
  description text,
  created_at TIMESTAMP
);

INSERT INTO projects (name, description) VALUES 
('tecky project group1', 'The best group project in tecky'),
('tecky group 2', 'excellent project');

INSERT INTO projects (name, description) VALUES 
('tecky group 3', 'excellent project');


ALTER TABLE projects ADD COLUMN company_id INTEGER; 
ALTER TABLE projects ADD CONSTRAINT fk_companyId FOREIGN key (company_id) REFERENCES companies(id);

ALTER TABLE projects ADD COLUMN user_id INTEGER; 
ALTER TABLE projects ADD CONSTRAINT fk_creatorId FOREIGN key(user_id) REFERENCES users(id);


SELECT * from projects;







