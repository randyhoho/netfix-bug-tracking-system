CREATE TABLE comments (
  id SERIAL PRIMARY KEY,
  Content text NOT NULL,
  Created_at TIMESTAMP
);

INSERT INTO comments (content, created_at) 
VALUES('will fix the bug asap', NOW()),
('dun know how to solve the bug, need Gordans help', NOW());

ALTER TABLE comments ADD COLUMN user_id integer;
ALTER TABLE comments ADD CONSTRAINT fk_userId FOREIGN key(user_id) REFERENCES users(id);

ALTER TABLE comments ADD COLUMN issue_id integer;
ALTER TABLE comments ADD CONSTRAINT fk_issueId FOREIGN key(issue_id) REFERENCES issue_reports(id); 

ALTER TABLE comments ALTER COLUMN Created_at TYPE timestamp with time zone;

SELECT * from comments ORDER BY created_at;


 