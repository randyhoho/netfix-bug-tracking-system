-- Create table
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  user_name TEXT NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  email_address VARCHAR (255) NOT NULL,
  password VARCHAR (255) NOT NULL,
  role Text NOT NULL
);

-- Sdd default user table
INSERT INTO users (user_name,first_name,last_name,email_address,password,role) VALUES
('a', 'a', 'a', 'a@gmail.com','$2a$10$TUjyCjhhGbtE9PQrdZATouk1Ev/hRFprQYl7PCxrOdYO./9NndkgO','Developer'),
('gordon','Gordon','Lau','gordon@tecky.io','$2a$10$zt1DKdAzqOc3d1x.YfFAr.cid8Ant..b9tQiZGar5CFbYxtWAvgZi','Developer'),
('randy','Randy','Ho','randyhoho2020@gmail.com','$2a$10$Ny0PTr5xrcQZU2Ps.oyZAua8H/.k5TvxxMHEO0fDObj7AsivG8cWW','Developer'),
('angela','Angela','Wong','wonglaichu@gmail.com','$2a$10$AVDLTcnWdXu8GfbkF8EULuNAGWbSKNiYn7VMjldI0gHNFR8ZmMFbW','Developer'),
('enson','Sin Pok','Yeung','yeungsinpok@gmail.com','$2a$10$Q.t5BbHKS7Joq3vNB1gzBeCG7ENVwxpIFcuJCAlPFiOc8XHxUMNtq','Developer'),
('oiver','Mei Yee','Chan','oliver@gmail.com','$2a$10$f/jQ0NCujlCfeLNSHIX.dOKCac7rVH4joTbPwZKSLrdIq33UuPL9.','Developer'),
('jack','Chun Kit','Leung','jack@gmail.com','$2a$10$sKrTFc7ugrx/V052TyFiauN5RJttAJ4gWq5oBjpXqJosaWEDnCxR2','Developer'),
('harry','Ka Lam','Lam','harry@gmail.com','$2a$10$Go7RNf9FsjxqDjBeDC9FFOggK86BU7u66kPnMaItDndUNEmFgV38W','User'),
('jacob','Chun Man','Chan','jacob@gmail.com','$2a$10$h9hesc4YafFaFZ0EJ/k0YuV2OwQUCe97B9AoIX5wGuUuu3lEdGcNm','User'),
('charlie','Man Kuan','Liu','charlie@gmail.com','$2a$10$lmoREg6gXvyzAZZoGRFxW.RbFXxvpbgxKFaLXFv6JjT/1t2EJUSMW','Developer'),
('thomas','Ming','Li','thomas@gmail.com','$2a$10$ydO65MDwcSaxSsKipO6t.O8Fs0H1cqFQNXzVGfaMF4hZIB3CH1jJ2','Developer'),
('george','Ka Ming','Shum','george@gmail.com','$2a$10$MkkCENp1mRA5vMDbxLObE.p0Limyav9/m84KN5eopenrMZaFlGf7m','User'),
('oscar','Ka Chun','Lee','oscar@gmail.com','$2a$10$cbTRa.9R8K6NV3O0XGYldeRHReUU3Ze0WtnSJUFHdlvTi.j4i71nC','Developer'),
('james','Shun La','Chan','james@gmail.com','$2a$10$3zu/600ePDpE1N2Oe1uwEefUh6cyyXMuIbYGa/RrauFezi/APSP8G','User'),
('wiliam','Jee King','Leung','wiliam@gmail.com','$2a$10$dmJIuqVdfCVyLxYC1AJL9uu4s6hBWtdpoXjvWYgRgIrKKvtFcFiGy','Developer'),
('ava','Ma Mee','Wong','ava@gmail.com','$2a$10$D0Rl/X6RmPu5MjKDTuhYp.AvNvSLrGBfwhusuUPeR0alKCp7zzQTe','User'),
('linda','Yee','Ma','linda@gmail.com','$2a$10$QBI9esqKLOATC/5M9.eZy.Irs.a/lGGZm2HsTnmt7p.CP9PvNH19q','User'),
('jessica','Ka Yee','Yip','jessica@gmail.com','$2a$10$sSGFtQgkF8HWjryup2NECeMvxZe0snqyHa/7UugCdook4tfTRYc3.','Developer'),
('sarah','Siu Ming','Lam','sarah@gmail.com','$2a$10$fsBKMmKahZgrQMQWHY45qOVZA.QWTi3TWttLNjgSG5m3C/TI4jT7m','User'),
('susan','Ka Sin','Wong','susan@gmail.com','$2a$10$RdFy2zNHMgiSY8VDV6uCle/v9eIEi7eauR9y.48G/AsBraf/X3LF','User')

-- password already Bcrypt, here is the password record for hunman read
-- ('a', 'a'),
-- ('Gordon''gordonpassword'),
-- ('Randy','randypassword'),
-- ('Angela','angelapassword'),
-- ('Enson','ensonpassword'),
-- ('Oiver','oliverpassword'),
-- ('Jack','jackpassword'),
-- ('Harry','harrypassword'),
-- ('Jacob','jacobpassword'),
-- ('Charlie','charliepassword'),
-- ('Thomas','thomaspassword'),
-- ('George','georgepassword'),
-- ('Oscar','oscarpassword'),
-- ('James','jamespassword'),
-- ('Wiliam''wiliampassword'),
-- ('Ava','avapassword'),
-- ('Linda','lindapassword'),
-- ('Jessica','jessicapassword'),
-- ('Sarah','sarahpassword'),
-- ('Susan','susanpassword')


SELECT * from users;


DELETE FROM users WHERE id > 20;


