CREATE TABLE companies (
  id serial PRIMARY KEY,
  name text
);

SELECT * FROM companies;