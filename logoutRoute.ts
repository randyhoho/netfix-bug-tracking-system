import express from 'express';


export const logoutRoute = express.Router();

logoutRoute.use('/logout', async (req, res) => {
    delete req.session['user'];
    res.redirect('/');
});