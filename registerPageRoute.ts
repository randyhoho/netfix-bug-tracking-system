import express from 'express';
import { client, io } from './app';
import { hashPassword } from './hash';
import { User } from './loginRoute';


export const registerPageRoute = express.Router();

registerPageRoute.route('/register')
    .get((req: any, res: { sendFile: (arg0: string) => void; }) => {
        res.sendFile(`${__dirname}/public/registerPage.html`);
    })

registerPageRoute.post('/register', async function (req, res) {
    const { first_name, last_name, user_name, role, email_address } = req.body;
    console.log(req.body)
    const password = req.body.password;
    const afterHashPassword = await hashPassword(password)
    const result = await client.query('SELECT user_name, email_address from users WHERE user_name = $1 or email_address = $2',[user_name, email_address]) 
    if (!first_name || !last_name || !user_name || !role || !email_address || !password) {
        res.status(409).json({ success: false, msg: "You should enter all info." })
        return
    }
    if (result.rows.length >= 1 ) {
        res.status(409).json({ success: false, msg: "Your username/ email already exists" })
    } else {
        io.emit('new-user', 'One more user join Netfix.');
        let user: User[] = (await client.query('SELECT * from users where user_name = $1',
        [user_name])).rows;
        req.session['user'] = req.body;
        console.log(user);
        await client.query(`INSERT INTO users (first_name,last_name,user_name,role,email_address,password) VALUES ($1,$2,$3,$4,$5,$6)`,
            [first_name, last_name, user_name, role, email_address, afterHashPassword])
        res.status(200).json({ success: true })
    }
})
