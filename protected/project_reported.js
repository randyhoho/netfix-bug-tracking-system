window.onload = function () {
    loadProjectContent();
}

async function loadProjectContent() {

    const search = new URLSearchParams(location.search); 
    const id = search.get('id');
    console.log(id);

    const res = await fetch(`/project_reported/${id}`);
    const result = await res.json();
    // console.log(projects.rows);
    let projects = result;

    console.log(projects)
    

    const projectContentBoard = document.querySelector('#projectContentBoard');
    for (let project of projects) {
        projectContentBoard.innerHTML = `
        <div class="card-body projectContent" data-id='${project.id}'>
            <label>Project name</label>
            <div class='projectTitle'>${project.name}</div>
            <label>Company</label>
            <div class='companyName'>${project.company}</div>
            <label>Participants</label>
            <div class='participants'>${project.participants}</div>
            <label>Description</label>
            <div class='description'>${project.description}</div>
        </div>
    `;
    }
}

