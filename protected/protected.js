// logout button

const logout = document.querySelector('.logout');

logout.onclick = async function(){
    const res = await fetch('/logout'); 
    if(res.status===200 ) {window.location = '/protected/logoutPage.html' 
} else {
    window.location = '/public/404.html' 
}};
