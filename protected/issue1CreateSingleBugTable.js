import { getBugList } from './issue1indexGetAll.js';

export async function createSingleBugTable() {
  const inputForm = document.getElementById('issue-form');

  inputForm.onsubmit = async (event) => {
    event.preventDefault();
    const inputFormData = {
      title: inputForm['title'].value,
      description: inputForm['description'].value,
      request_type:inputForm['request_type'].value,
      priority:inputForm['priority'].value,
      browser:inputForm['browser'].value,
      status: inputForm['status'].value
    };
    const res = await fetch(`/bugTable/p`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(inputFormData),
    });
    const result = await res.json();
    inputForm.reset();
    getBugList();
  };
}
