import { getBugList } from './issue1indexGetAll.js';

export async function editSingleBugTable(bug = {}) {

  // const inputForm = document.getElementById('editsubmitbutton');
  // document.getElementById('id').innerHTML = window.location.search
 
  // inputForm.querySelector(`[name=id]`).value = bug.id
  // inputForm.querySelector(`[name=title]`).value = bug.title
  // inputForm.querySelector(`[name=description]`).value = bug.description
  // inputForm.querySelector(`[name=request_type]`).value = bug.request_type
  // inputForm.querySelector(`[name=priority]`).value = bug.priority
  // inputForm.querySelector(`[name=browser]`).value = bug.browser

  inputForm.onsubmit = async (event) => {
    event.preventDefault();
    const formToDatabase = {
      id: inputForm['id'].value,
      title: inputForm['title'].value,
      description: inputForm['description'].value,
      request_type: inputForm['request_type'].value,
      priority: inputForm['priority'].value,
      browser: inputForm['browser'].value,
    };
    const res = await fetch(`/bugTable/put/${formToDatabase.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(formToDatabase),
    });
    const result = await res.json();
    // inputForm.reset();
    // getBugList();
  };
}
