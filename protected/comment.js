const form = document.querySelector('#commentForm');

form.addEventListener('submit', async function (event) {
    event.preventDefault();

    const form = event.target;
    const formObject = {
        commentContent: form.commentContent.value
    };


    const res = await fetch('/comments', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObject)
    });
    const result = await res.json();

    // console.log(JSON.stringify(result));
    if (res.status === 200) {
        form.reset();
        // $('.toast').toast('show');
    } else {
        console.log('failed to add comment')
    }
    loadComments();
});

async function loadComments() {

    const res = await fetch('/comments');
    const comments = await res.json();

    const commentBoard = document.querySelector('#commentBoard');
    commentBoard.innerHTML = '';
    

    for (let comment of comments) {
        // console.log(comment)
        commentBoard.innerHTML += `
        <div class='comment-box col-lg-12' data-id='${comment.id}'>
            <div class='message-row' >
                <div class='comment-user'>Peter</div>
                <div class='comment-content'>${comment.content}</div>
             </div>
             <i class="bi bi-x" id='delete'></i>
            <div class='time'>${comment.created_at}</div>
        </div>
        `
    };

    const commentDivs = document.querySelectorAll('.comment-box');

    for (let commentDiv of commentDivs) {

        commentDiv.querySelector('.bi-x').onclick = async function (event) {
            
            const deleteLabel = event.target;
            
            const comment = deleteLabel.closest('.comment-box');
            const commentId = comment.getAttribute('data-id');
            
            const res = await fetch(`/comments/${commentId}`, {
                method: "DELETE",
            });

            console.log('testing');

            const result = await res.json();
            loadComments();
        }
    };
};

loadComments();





