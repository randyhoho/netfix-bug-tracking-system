import { getBugList, renderInfoToEditForm } from './issue1indexGetAll.js';
import { createSingleBugTable } from './issue1CreateSingleBugTable.js';
import { editSingleBugTable } from './issue1editSingleBugTable.js';

window.onload = () => {
    console.log(  localStorage.getItem('editId')  )
    renderInfoToEditForm()
    getBugList();
    createSingleBugTable();
    editSingleBugTable();
  };

function setLocalStorage(){
  localStorage.getItem('editId');
  
}