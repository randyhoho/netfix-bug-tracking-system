

// confirm button function
const next = document.querySelector('.confirmedButton');

next.onclick = async function(){
  window.location = '/protected/home.html'
};

const user_name = document.querySelector('.user_name');
const email_address = document.querySelector('.email_address');
const role = document.querySelector('.role');

window.onload = ()=>{
    loadDataLogin();
  
}

async function loadDataLogin(){
    const res = await fetch('/businessCardLogin',
            {
            method: "POST"
            }
        );
    const result = await res.json();
    console.log(result)

document.getElementById("user_name").innerHTML += result["userData"]["user_name"];
document.getElementById("email_address").innerHTML += result["userData"]["email_address"];
document.getElementById("role").innerHTML += result["userData"]["role"];
}

