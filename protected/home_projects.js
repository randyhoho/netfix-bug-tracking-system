window.onload = function () {
    loadProject();
    loadUsername();
}

// ok??
async function loadUsername() {
    const res = await fetch('/loginFormAll');
    const users = await res.json();

    const userMessage = document.querySelector('.userMessage');
    userMessage.innerHTML = '';

    for (let user of users) {
        usernameMessage.innerHTML += `
        <h1 class="text" id="welcomeMessage" data-id='${user.id}'>Welcome, ${user.user_name}</h1>
        <p class="subtitle text">${user.role}</p>
        `
    }
}


async function loadProject() {
    const res = await fetch('/projects');
    const projects = await res.json();
    // console.log(projects);
    const projectBoard = document.querySelector('#projectBoard');
    projectBoard.innerHTML = '';

    // const location = new URL('/project_reported.html?id=${project.id}');
    // const search = new URLSearchParams(location.search);

    for (let project of projects) {
        projectBoard.innerHTML += `
        <div class='project' data-id='${project.id}'>
        <i class="bi bi-x"></i>
        <a href="/project_reported.html?id=${project.id}" class="box">${project.name}</a>
      </div>`
    }

    


    const projectDivs = document.querySelectorAll('.project');

    for (let projectDiv of projectDivs) {

        projectDiv.querySelector('.bi-x').onclick = async function (event) {

            const deleteLabel = event.target;
            const project = deleteLabel.closest('.project');
            const projectId = project.getAttribute('data-id');
            console.log(projectId);

            const res = await fetch(`/projects/${projectId}`, {
                method: "DELETE",
            });

            const result = await res.json();
            loadProject();
        }
    };
};


// Result to be verified
// async function getUsername() {
//     const res = await fetch('/home'); 
//     const result = await res.json();

//     result.username = document.querySelector('#welcomeMessage').innerHTML;
// }

const logout = document.querySelector('.logout');

logout.onclick = async function(){
    const res = await fetch('/logout'); 
    if(res.status===200 ) {window.location = '/protected/logoutPage.html' 
} else {
    window.location = '/public/404.html' 
}};
