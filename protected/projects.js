

const form = document.querySelector('#projectForm');

form.addEventListener('submit', async function(event) {
    event.preventDefault();

    const form = event.target;

    const formObject = {
        projectName: form.projectName.value,
        company: form.company.value,
        participants: form.participants.value,
        description: form.projectDescription.value
    }; 

    const res = await fetch('/projects', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObject)
    }); 

    const result = await res.json();
    console.log(result);

    
    
    if(res.status === 200) {
        form.reset();
        $('.toast').toast('show');
    } else {
        console.log('project submit failed')
    }

});



